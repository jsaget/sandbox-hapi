# Node, Elasticsearch & HAPI foundation project

Simple server foundation project with node.js / HAPI and a build system based on Gulp.

* The build system can build one or more front
* You can use the Front framework of your choice.
* Elasticsearch integrated with [ESObject](https://www.npmjs.com/package/esobject). Use to store Session (and more if you need)
* Session management with [Yar](https://github.com/hapijs/yar "Yar - HAPI Plugin") (HAPI Plugin). Stored in Elasticsearch


## Table of contents
[TOC]

## Build system
This build system is based on *Gulp* with *Glou* library

### Configuration

Build system configuration is written in  **config.yaml**. It used to specify how to build your application.

#### Config file example
```
clients:
  front1:
    jshint:
      rc: 'clients/front1/.jshintrc'
      reporter: 'jshint-stylish'
      failReporter: 'fail' # 'fail' || failImmediately'
    jscs:
      rc: 'clients/front1/.jscsrc'
      failReport: 'fail' # fail' || failImmediately'
    js:
      lib:
        head:
        body:
        - node_modules/angular/angular.min.js
        - node_modules/angular-ui-bootstrap/ui-bootstrap-tpls.min.js
      app:
        head:
        body:
        - clients/front1/app/index.js
    styles:
      lib:
      - node_modules/font-awesome/css/font-awesome.min.css
      - node_modules/bootstrap/dist/css/bootstrap.min.css
      app:
      - 'clients/front1/public/styles/main.styl'
    copy:
    - from: node_modules/font-awesome/fonts/*
      to: fonts
    - from: node_modules/bootstrap/fonts/*
      to: fonts
    templates:
      module: ru
      files:
      - 'clients/front1/**/*.{html,jade}'
      - '!**/layouts/**'

server:
  lr:
    port: 35729
  jshint:
    rc: 'server/.jshintrc'
    reporter: 'jshint-stylish'
    failReporter: 'fail'
  jscs:
    rc: 'server/.jscsrc'
    failReport: 'fail'
  entry: server/server.js
  files:
  - server/**/*
  statics:
  - build/**/*

i18n: '(!(node_modules))/**/locale/*.{json,yaml}'
```

#### Front

**Clients** object contains the object configuration for each front that you need to build. Add an attribut (key, object) for each client you need to build. In previous example, we have one front call *frontName*

In the front configuration object define some properties:
* **jshint** : jshint  properties for gulp configuration
* **jscs** : jscs properties for gulp configuration
* **js** : JavaScript file that will be minify and concatenate for your build version of you front 
* **styles** : CSS file to concatenate. Current version use LESS only
* **copy** : files to copy in your build version
* **template**: js template that will be concatenate in js file 

##### jshit & jscs configuration
```
jshint:
  rc: 'clients/front1/.jshintrc'
  reporter: 'jshint-stylish'
  failReporter: 'fail' # 'fail' || failImmediately'
jscs:
  rc: 'clients/front1/.jscsrc'
  failReport: 'fail' # fail' || failImmediately'
```

You can specify for jshint and jscs some informations like:
* rc: configuration file
* reporter: type of the reporter to use (for jshint)
* failReporter: how to manage error while checking


```
js:
  lib:
    head:
    body:
    - node_modules/angular/angular.min.js
    - node_modules/angular-ui-bootstrap/ui-bootstrap-tpls.min.js
  app:
    head:
    body:
    - clients/front1/app/index.js
```

```
styles:
  lib:
  - node_modules/font-awesome/css/font-awesome.min.css
  - node_modules/bootstrap/dist/css/bootstrap.min.css
  app:
  - 'clients/front1/public/styles/main.styl'
```

##### Copy
**Copy** task copy files from a source to a destination.

###### Object definition
Copy is an array of object whose define how to copy those files.
```
copy = [{
  from: '',
  to: '',
  srcOptions: {}
}, 
{...}]
```
* *from* : path of the file to copy, from project root
* *to* : path of destination of the file, in frontName build directory (i.e: build/frontName)
* *srcOptions* :  See glou.src option 

###### Example 
```
copy:
- from: node_modules/font-awesome/fonts/*
  to: fonts
- from: node_modules/bootstrap/fonts/*
  to: fonts
```
This copy rules will copy fonts from font-awesome and bootstrap in the build/frontName/fonts folder.

```
templates:
  module: ru
  files:
  - 'clients/front1/**/*.{html,jade}'
  - '!**/layouts/**'
```

#### server

##### Jshint, Jscs & Live Reload
```
lr:
  port: 35729
jshint:
  rc: 'server/.jshintrc'
  reporter: 'jshint-stylish'
  failReporter: 'fail'
jscs:
  rc: 'server/.jscsrc'
  failReport: 'fail'
```

##### Node server entry point
```
entry: server/server.js
```

##### Server files
This information is used for the watcher for rebuild on the fly.
```
files:
- server/**/*
```

##### Hum, idk, something probably usefull
```
statics:
- build/**/*
```

### Available command
**/!\ TODO /!\ **
default
clean

server-jscheck
server-start
watch

frontname-jsfront
frontname-copy
frontname-style

run-server => server-jscheck, server-start

run => run-server, watch

build-*frontname* => *frontname*-jsfront, *frontname*-copy, *frontname*-style
build => *front\**-jsfront, *front\**-copy, *front\**-style
run


## Server configuration
### Configuration file
Properties for node server configuration is specified in server/conf/*

#### default.js

```
 elasticsearch: {
    host: 'http://localhost:9200',
    index: 'sandbox',
    log: 'warning',

    initIndexParams: {
      settings: {
        number_of_replicas: 0,
        number_of_shards: 2,
      },
    },
  },
```


### HAPI plugins
HAPI plugin are load at startup server.
You can add or remove plugin here.

#### Example
```
  plugins: [{
    plugin: 'inert', // plugin name for external or path for internal
    pluginOptions: undefined,
    registrationParameters: undefined,
  }, {
    plugin: 'yar',
    pluginOptions: {
      storeBlank: false,
      maxCookieSize: 0,
      cache: {
        expiresIn: 60 * 60 * 1000,
        segment: 'session',
      },
      cookieOptions: {
        password: 'kb281tjx84a8qLYWJArq9ZUczYjmwCzJ',
        isSecure: process.env.NODE_ENV !== 'development',
      },
    },
    registrationParameters: undefined,
  }, {
    plugin: './hapi-plugins/statics',
    pluginOptions: {dir: './front1'},
    registrationParameters: {
      routes: {
        // prefix: '/client',
        // vhost: 'b2c.localhost:3000',
      },
    },
  }, {
    plugin: './hapi-plugins/statics',
    pluginOptions: {dir: './front2'},
    registrationParameters: {
      routes: {
        prefix: '/admin',
//        vhost: 'schools.localhost:3000',
      },
    },
  }],
```

#### Plugin definition Object
```
{
  plugin: 'inert',
  pluginOptions: undefined,
  registrationParameters: undefined,
}
```
 
 * **plugin** : *Plugin name* or *path* of your custom plugin declared in your project (with current directory to server folder). This is field is used to require the plugin
 * **pluginOptions** : Object containing your data that will be pass to the plugin option when registered
 * **registrationParameters** : Object containing parameters for HAPI plugin registration. 
 ``` The options object is used by hapi and is not passed to the plugin(s) being loaded. It allows you to pre-select servers based on one or more labels, as well as apply vhost or prefix modifiers to any routes that your plugins register.```

For more information, see '***Plugin options***' and '***Loading a plugin***' in [HAPI plugin documentation](http://hapijs.com/tutorials/plugins)

#### Default Plugins
##### Statics (custom plugin)
This plugin is used to managed static files routes for multiple fronts (with prefix or vhost)

More information in HAPI documentation
See [*server.register(plugins, [options], [callback])* method](http://hapijs.com/api#serverregisterplugins-options-callback)
See [*Plugin options* Section](http://hapijs.com/tutorials/plugins#plugin-options)


## How to use this ?

Create your server modules in app-modules.
HAPI routes are loaded automatically. For this, each route **must** match the following syntax: *.route.js
Elasticsearch is used via ESObject. Those objects are also created automatically at startup. Each ES Object must match : *.esobject.js

Use RS to *restart* the server or *exit* or *stop* to stop it.

You must change the ***password*** of Yar plugin in the cookie options.

ES folder contains ES client declaration.
hapi folder: contains custom hapi plugins and hapi help

