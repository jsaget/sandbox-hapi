'use strict';

require('./app')
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('app', {
      abstract: true,
      url: '',
      resolve: {
        user: function(AuthService) {
          return AuthService.getCurrentUser();
        },
      },
      controller: indexController,
    })
    .state('app.login', {
      url: '/auth/login',
      templateUrl: 'components/auth/login/login.tpl.html',
//      controller: homeController,
    })
    .state('app.home', {
      url: '/home',
      templateUrl: 'components/home/home.tpl.html',
    })
  ;

  // for unmatch URL, redirect to /home
  $urlRouterProvider.otherwise('/home');
})

.run(function($rootScope) {

  $rootScope.$on('$stateChangeStart', function(event, toState, params) {
    console.log('START TO LOAD:', toState);

    if (toState.redirectTo) {
      event.preventDefault();
      $state.go(toState.redirectTo, params);
    }
  });

  $rootScope.$on('$locationChangeSuccess', function(event, toState) {
    console.log('location change sucess:', toState);
  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState) {
    console.log('LOADED:', toState);
  });

  $rootScope.$on('$stateChangeError', function(event, toState, b, c, e, error) {
    console.log('FAILED TO LOAD:', toState, error);
  });

  $rootScope.$on('$stateNotFound', function(event, toState) {
    console.log('FAILED NOT FOUND:', toState);
  });
})
;
