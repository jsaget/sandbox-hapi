'use strict';

require('./locales/service');
require('./authentication/service');

var indexController = require('./controller');
var homeController = require('./home/home_controller');
var loginController = require('./contact/contact_controller');

require('./app')
.config(function($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider
    .state('app', {
      abstract: true,
      url: '',
//      template: '<div ui-view></div>',
      resolve: {
        user: function(AuthService) {
          return AuthService.getCurrentUser();
        },
      },
      controller: indexController,
    })
    .state('auth', {
      url: '/home',
      templateUrl: 'home/home.tpl.html',
      controller: homeController,
      i18n: ['file!home'],
    })
    .state('app.home', {
      url: '/home',
      templateUrl: 'home/home.tpl.html',
      controller: homeController,
      i18n: ['file!home'],
    })
  ;
  // for unmatch URL, redirect to /home
  $urlRouterProvider.otherwise('/home');
})
.run(function($rootScope) {
  'ngInject';

  $rootScope.$on('$stateChangeStart', function(event, toState,params) {
    console.log('START TO LOAD:', toState);

    if (toState.redirectTo) {
      evt.preventDefault();
      $state.go(toState.redirectTo, params)
    }
  });

  $rootScope.$on('$locationChangeSuccess', function(event, toState) {
    console.log('location change sucess:', toState);
  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState) {
    console.log('LOADED:', toState);
  });

  $rootScope.$on('$stateChangeError', function(event, toState, b, c, e, error) {
    console.log('FAILED TO LOAD:', toState, error);
  });

  $rootScope.$on('$stateNotFound', function(event, toState) {
    console.log('FAILED NOT FOUND:', toState);
  });
})
;
