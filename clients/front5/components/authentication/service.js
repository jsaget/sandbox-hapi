'use strict';

require('./ru')
.service('AuthService', function($http) {
  'ngInject';

  this.authenticate = function(credentials) {
    return $http.post('/api/auth/login', credentials)
      .get('data')
      .then(function(user) {
        setCurrentUser(user);
        return user;
      })
    ;
  };

  this.disconnect = function() {
    return $http.get('/api/auth/logout');
  };
});
