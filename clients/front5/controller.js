'use strict';

require('./authentication/service');

module.exports = function($scope, $state, AuthService) {
  'ngInject';

  $scope.disconnect = function() {
     return AuthService.disconnect()
       .then(function() {
         $scope.user = null;
         $state.go('app');
       })
     ;
   };

  $scope.locales = {
    'Français': 'fr-FR',
    English: 'en-US',
  };
};
