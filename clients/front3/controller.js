'use strict';

module.exports = function($scope) {
  'ngInject';

  $scope.locales = {
    'Français': 'fr-FR',
    English: 'en-US',
  };
};
