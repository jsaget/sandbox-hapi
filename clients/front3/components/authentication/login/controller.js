
require('./authentication/service');

module.exports = function($scope, $state, $timeout, user, notifications, AuthService) {
  'ngInject';

  $scope.user = user;

  $scope.login = function(user) {
    return AuthService.authenticate(user)
      .then(function(res) {
        var username = res.username;
        $scope.user = res;
      })
      .catch(function(err) {
        notifications.error('User Login', err.data.message);
      })
    ;
  };
};
