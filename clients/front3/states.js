'use strict';

require('./locales/service');
require('./authentication/service');

var indexController = require('./index_controller');
var homeController = require('./home/home_controller');
var contactController = require('./contact/contact_controller');

var aboutController = require('./about/about_controller');
var faqController = require('./faq/faq_controller');
var cguController = require('./cgu/cgu_controller');
var privateController = require('./private/private_controller');

var toeicController = require('./toeic/toeic_controller');
var toeflController = require('./toefl/toefl_controller');
var hskController = require('./hsk/hsk_controller');

require('./ge')
.config(function($stateProvider, $urlRouterProvider) {
  'ngInject';

  $stateProvider
    .state('app', {
      abstract: true,
      url: '',
      templateUrl: 'app.tpl.html',
      i18n: ['file!index'],
      resolve: {
        user: function(AuthService) {
          return AuthService.getCurrentUser();
        },
        currentLng: function(localStorageService) {
          return localStorageService.get('currentLng') || {};
        },
      },
      controller: indexController,
    })
    .state('app.home', {
      url: '/home',
      templateUrl: 'home/home.tpl.html',
      controller: homeController,
      i18n: ['file!home'],
    })
    .state('app.about', {
      url: '/about',
      templateUrl: 'about/about.tpl.html',
      controller: aboutController,
      i18n: ['file!about'],
    })
    .state('app.contact', {
      url: '/contact',
      templateUrl: 'contact/contact.tpl.html',
      controller: contactController,
      i18n: ['file!contact'],
    })
    .state('app.cgu', {
      url: '/cgu',
      templateUrl: 'cgu/cgu.tpl.html',
      controller: cguController,
      i18n: ['file!cgu'],
    })
    .state('app.private', {
      url: '/private',
      templateUrl: 'private/private.tpl.html',
      controller: privateController,
      i18n: ['file!private'],
    })
    .state('app.faq', {
      url: '/faq',
      templateUrl: 'faq/faq.tpl.html',
      controller: faqController,
      i18n: ['file!faq'],
    })
    .state('app.toeic', {
      url: '/toeic',
      templateUrl: 'toeic/toeic.tpl.html',
      controller: toeicController,
      i18n: ['file!toeic'],
    })
    .state('app.toefl', {
      url: '/toefl',
      templateUrl: 'toefl/toefl.tpl.html',
      controller: toeflController,
      i18n: ['file!toefl'],
    })
    .state('app.hsk', {
      url: '/hsk',
      templateUrl: 'hsk/hsk.tpl.html',
      controller: hskController,
      i18n: ['file!hsk'],
    })
  ;
  // for unmatch URL, redirect to /home
  $urlRouterProvider.otherwise('/home');
})
.run(function($rootScope) {
  'ngInject';

  $rootScope.$on('$stateChangeStart', function(event, toState) {
    console.log('START TO LOAD:', toState);
  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState) {
    console.log('LOADED:', toState);
  });

  $rootScope.$on('$stateChangeError', function(event, toState, b, c, e, error) {
    console.log('FAILED TO LOAD:', toState, error);
  });

  $rootScope.$on('$stateNotFound', function(event, toState) {
    console.log('FAILED NOT FOUND:', toState);
  });
})
;
