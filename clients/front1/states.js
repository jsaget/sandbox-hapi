'use strict';

require('./app')
.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('app', {
      abstract: true,
      url: '',
      template: '<div ui-view></div>',
      resolve: {
        test: function() {
          console.log('states.js:13', 'TEST');
        },
        user: function(AuthService) {
          console.log('states.js:12', 'ON RESOLVE');
          return AuthService.getCurrentUser();
        },
      },
      onEnter: function($stateParams) {
        console.log('states.js:16', 'ON ENTER');
      },
    })
    .state('app.login', {
      url: '/auth/login',
      templateUrl: 'components/auth/login/login.tpl.html',
//      controller: homeController,
    })
    .state('app.home', {
      url: '/home',
      templateUrl: 'components/home/home.tpl.html',
    })
  ;

  // for unmatch URL, redirect to /home
  $urlRouterProvider.otherwise('/home');
})

.run(function($rootScope) {

  $rootScope.$on('$stateChangeStart', function(event, toState, params) {
    console.log('START TO LOAD:', toState);

    if (toState.redirectTo) {
      event.preventDefault();
      $state.go(toState.redirectTo, params);
    }
  });

  $rootScope.$on('$locationChangeSuccess', function(event, toState) {
    console.log('location change sucess:', toState);
  });

  $rootScope.$on('$stateChangeSuccess', function(event, toState) {
    console.log('LOADED:', toState);
  });

  $rootScope.$on('$stateChangeError', function(event, toState, b, c, e, error) {
    console.log('FAILED TO LOAD:', toState, error);
  });

  $rootScope.$on('$stateNotFound', function(event, toState) {
    console.log('FAILED NOT FOUND:', toState);
  });
})
;
