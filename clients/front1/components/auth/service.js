'use strict';

require('./app')
.service('AuthService', function($http, $state, localStorageService) {
  'ngInject';

  var currentUser;


  // this.isAuthenticate = function($q, user, $state, $timeout) {
  //         if (user.isAuthenticated()) {
  //           // Resolve the promise successfully
  //           return $q.when()
  //         } else {
  //           // The next bit of code is asynchronously tricky.

  //           $timeout(function() {
  //             // This code runs after the authentication promise has been rejected.
  //             // Go to the log-in page
  //             $state.go('logInPage')
  //           })

  //           // Reject the authentication promise to prevent the state from loading
  //           return $q.reject()
  //         }
  //       }
  // }


  function setCurrentUser(user) {
    var oldUser = currentUser;
    currentUser = user;

    if ($state.current.abstract || currentUser === oldUser ||
      (currentUser && oldUser && currentUser._id === oldUser._id))
      return;

    return $state.reload();
  }

  this.getCurrentUser = function() {
    return currentUser ||
      $http.get('/api/user/_me').get('data')
        .catch(function() {
          return null;
        })
        .tap(setCurrentUser)
    ;
  };

  this.authenticate = function(credentials) {
    return $http.post('/api/auth/authenticate', credentials)
      .get('data')
      .then(function(user) {
        setCurrentUser(user);
        return user;
      })
    ;
  };

  this.signin = function(user, invitation) {
    var url = '/api/user/' + user.username;
    if (invitation)
      url = url + '?schoolId=' + invitation._parent + '&invitationId=' + invitation._id;

    return $http.put(url, user).get('data');
  };

  this.disconnect = function() {
    // localStorageService.clearAll();
    localStorageService.remove('basket');
    return $http.get('/api/auth/disconnect')
      .then(setCurrentUser(null))
    ;
  };

  this.changePassword = function(credentials) {
    return $http.post('/api/auth/password/change', credentials).get('data');
  };

  this.sendUsernameRecoveryMail = function(rawForm) {
    return $http.post('/api/auth/user/recovery', rawForm).get('data');
  };

  this.confirmEmail = function(token) {
    return $http.put('/api/auth/email_confirmation/' + token._id).get('data');
  };

  this.getEmailToken = function(tokenId) {
    return $http.get('/api/auth/email_confirmation/' + tokenId).get('data');
  };

  this.sendConfirmEmail = function(data) {
    return $http.post('/api/auth/email_confirmation/send', data).get('data');
  };

  this.sendRecoveryMail = function(user) {
    return $http.post('/api/auth/password/recovery', user).get('data');
  };

  this.resetPassword = function(user, tokenId) {
    return $http.post('/api/auth/password/reset/' + tokenId, user).get('data');
  };

  this.getPasswordToken = function(tokenId) {
    return $http.get('/api/auth/password/reset/' + tokenId).get('data');
  };
});
