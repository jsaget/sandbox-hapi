'use strict';

var _ = require('lodash');
var Promise = require('bluebird');
var gulp = require('gulp');
var runSequence = require('run-sequence');
var del = require('del');

var config = require('./gulp/configFileReader');
var copy = require('./gulp/copy');
var styles = require('./gulp/styles');
var jsfront = require('./gulp/js-front');
var jsserver = require('./gulp/js-server.js');
require('./gulp/watcher');

const defaultConfig = {
  jshint: {
    rc: '.jshintrc',
    reporter: 'jshint-stylish',
    failReporter: 'fail',
  },
  jscs: {
    rc: '.jscsrc',
    failReporter: 'fail',
  },
  lr: {
    port: 35729,
  }
};

var frontCheck = [];
var frontCopy = [];
var frontstyle = [];

// generate gulp front tasks
// frontname + '-jsfront'
// frontname + '-style'
// frontname + '-copy'
// frontname + '-jscheck'
// 'build-' + frontname
_.forEach(config.clients, function(value, key) {
  frontCheck.push(key + '-jsfront');
  frontstyle.push(key + '-style');
  frontCopy.push(key + '-copy');
  gulp.task('build-' + key, [key + '-jsfront', key + '-style', key + '-copy']);

  var cfgObject = _.extend({}, defaultConfig, {frontName: key}, value);
  copy.copy(key, cfgObject);
  jsfront.buildFront(key, cfgObject);
  styles.buildStyles(key, cfgObject);
});

// gulp server tasks
// 'watch'
// 'server-jscheck'
// 'server-start'
gulp.task('run-server', ['server-jscheck', 'server-start']);
var cfgObject = _.extend({}, defaultConfig, config.server);
jsserver.startInstance(cfgObject);

gulp.task('build', function(cb) {
  runSequence(
    frontCheck,
    frontCopy,
    frontstyle,
    cb
  );
});
gulp.task('run', function(cb) {
  runSequence(
    'run-server',
    'watch',
    cb
  );
});

gulp.task('clean', function(cb) {
   Promise.resolve(del(['build/**'])).nodeify(cb);
});

gulp.task('default', function(cb) {
  runSequence(
    'clean',
    'build',
    'run',
    cb
  );
});
