'use strict';

const Promise = require('bluebird');
const _ = require('lodash');
const glob = Promise.promisify(require('glob'));
const Path = require('path');
const Hapi = require('hapi');
const chalk = require('chalk');
const conf = require('../conf');
const cache = require('./hapi-plugins/catbox').cache;

var server;

module.exports.newInstance = function() {
  server = new Hapi.Server({
    app: {
      mode: process.env.NODE_ENV || 'development',
    },
    cache: cache,
  });

  server.connection({
    port: process.env.PORT || 3000,
    host: 'localhost',
  });

  Promise.promisifyAll(server);

  return server;
};

module.exports.loadRoutes = function loadHapiRoutes() {
  const cwd = Path.resolve(__dirname, '..', 'app-modules');

  return Promise.try(function() {
      return glob('**/*.route.js', {cwd: cwd});
    })
    .each(function(elt) {
      var route = require(Path.resolve(cwd, elt));
      try {
        server.route(route);
        console.log('Route: ', route.path, '(' + route.method + ')');
      } catch (e) {
        throw new Error('Cannot load route ' + elt + ':\n' + e.stack.replace(/^/gm, chalk.grey('  > ')));
      }
    })
  ;
}

module.exports.loadPlugins = function() {
  return Promise.each(conf.hapiPlugins, function(pluginConfig) {
    var plugin = {};
    try {
      plugin = require(pluginConfig.plugin);
    } catch (e) {
      throw new Error('Cannot load plugin ' + pluginConfig.plugin + ':\n' + e.stack.replace(/^/gm, chalk.grey('  > ')));
    }

    if (pluginConfig.pluginOptions) {
      plugin.options = pluginConfig.pluginOptions;
    }

    console.log('Plugin: ' + pluginConfig.plugin);
    if (pluginConfig.registrationParameters)
      return server.registerAsync(plugin, pluginConfig.registrationParameters);
    return server.registerAsync(plugin);
  });
};
