'use strict';

var esEngine = require('./es_engine');
var esClient = require('../../../es').client;
var conf = require('../../../conf');

module.exports.cache = {
  engine: esEngine,
  client: esClient,
  partition: conf.elasticsearch.index,
};
