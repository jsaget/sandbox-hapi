'use strict';

var EsConnection = module.exports = function ESConnection(options) {
  this.partition = options.partition;
  this.client = options.client;
};

EsConnection.prototype.start = function(callback) {
  callback();
};

EsConnection.prototype.stop = function() {};

EsConnection.prototype.isReady = function() {
  // this.client.ping();
  return true;
};

EsConnection.prototype.validateSegmentName = function(name) {
  if (!name) {
    return new Error('Empty string');
  }

  if (name.indexOf('\0') !== -1) {
    return new Error('Includes null character');
  }

  return null;
};

EsConnection.prototype.get = function(key, callback) {
  this.client.get({
    index: this.partition,
    type: key.segment,
    id: key.id
  })
    .then(function(res) {
      return {
        item: res._source.data,
        stored: new Date(res._source.updatedAt),
        ttl: res._ttl
      };
    })
    .catch(function(err) {
      return err.status === 404;
    }, function() {
      return null;
    })
    .nodeify(callback)
  ;
};

EsConnection.prototype.set = function(key, value, ttl, callback) {
  this.client.index({
      index: this.partition,
      type: key.segment,
      id: key.id,
      ttl: ttl + 'ms',
      body: {
        data: value,
        updatedAt: new Date(),
      },
    })
    .then(function() {
      callback();
    })
//    .return()
//    .asCallback(callback)
  ;
};

EsConnection.prototype.drop = function(key, callback) {
  this.client.delete({
    index: this.partition,
    type: key.segment,
    id: key.id,
  })
  .then(function(res) {
    if (callback)
      return callback(res);
    else
      return res;
  })
//    .return()
//    .asCallback(callback)
  ;
};
