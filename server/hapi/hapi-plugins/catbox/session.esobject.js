'use strict';

var esobject = require('esobject');
var config = require('../../../conf');
var esClient = require('../../../es').client;

module.exports = esobject.create({
  db: {
    client: esClient,
    index: config.elasticsearch.index,
    type: 'session',
  },

  mapping: __dirname + '/session.mapping.yaml',
});
