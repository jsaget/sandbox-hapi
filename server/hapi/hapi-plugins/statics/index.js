'use strict';

var path = require('path');
var accepts = require('accepts');
var uuid = require('node-uuid');


// Plugin dependencies
// http://hapijs.com/api#serverdependencydependencies-after
var register = module.exports.register = function register(server, options, next) {
  var routeId = 'statics-' + uuid.v4();

  // serve general static files
  server.route({
    method: 'GET',
    path: '/{param*}',
    handler: {
      directory: {
        path: options.dir,
        lookupCompressed: true,
      },
    },
    config: {id: routeId},
  });
  // try to catch the 404 to return the index
  server.ext('onPreResponse', function(request, reply) {
    // If the route was not our catch all, continue
    if (((request.route || {}).settings || {}).id !== routeId)
      return reply.continue();

    // If the response is not a 404, continue
    if (!request.response.isBoom || request.response.output.statusCode !== 404)
      return reply.continue();

    // If we are not asking for HTML, continue
    if (!accepts(request.raw.req).type('html'))
      return reply.continue();

    // If the request contains an extension, continue
    if (path.extname(request.path))
      return reply.continue();

    // Otherwise, return the index
    return reply.file(path.join(options.dir, 'index.html'));
  });

  return next();
};

// module.exports.register.attributes
register.attributes = {
  name: 'statics',
  version: '1.0.0',
  multiple: true,
  dependencies: 'inert',
};
