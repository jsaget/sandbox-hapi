'use strict';

var path = require('path');

module.exports = {
  elasticsearch: {
    host: 'http://localhost:9200/',
    apiVersion: process.env.ELASTICSEARCH_API_VERSION || '2.2',
    log: 'warning',
  },
};