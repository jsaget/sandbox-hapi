'use strict';

var _ = require('lodash');

var defaultConfig = require('./default');

var envConfigFile = './' + (process.env.NODE_ENV || 'development');
var envConfig = {};
try {
  envConfig = require(envConfigFile);
}
catch(e) {
  console.error('Environment conf file \'' + envConfigFile + '\' not found!');
}

module.exports = _.merge({}, defaultConfig, envConfig);
