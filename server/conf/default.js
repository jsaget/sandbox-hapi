'use strict';

var path = require('path');

module.exports = {
  elasticsearch: {
    host: 'http://localhost:9200',
    index: 'sandbox',
    log: 'warning',
    apiVersion: '2.2',

    initIndexParams: {
      settings: {
        number_of_replicas: 0,
        number_of_shards: 2,
      },
    },
  },

  hapiPlugins: [{
    plugin: 'inert',
    pluginOptions: undefined,
    registrationParameters: undefined,
  }, {
    plugin: 'yar',
    pluginOptions: {
      storeBlank: false,
      maxCookieSize: 0,
      cache: {
        expiresIn: 60 * 60 * 1000,
        segment: 'session',
      },
      cookieOptions: {
        password: 'kb281tjx84a8qLYWJArq9ZUczYjmwCzJ',
        isSecure: process.env.NODE_ENV !== 'development',
      },
    },
    registrationParameters: undefined,
  }, {
    plugin: './hapi-plugins/statics',
    pluginOptions: {dir: './front1'},
    registrationParameters: {
      routes: {
        // prefix: '/client',
        // vhost: 'b2c.localhost:3000',
      },
    },
  }, {
    plugin: './hapi-plugins/statics',
    pluginOptions: {dir: './front2'},
    registrationParameters: {
      routes: {
        prefix: '/admin',
//        vhost: 'schools.localhost:3000',
      },
    },
  }],
};
