'use strict';

const myHapi = require('./hapi');
const es = require('./es');

const server = myHapi.newInstance();

es.initIndex()
    .tap(myHapi.loadPlugins)
    //.tap(myHapi.loadRoutes)
    .return(server)
    .tap(function() {
      server.startAsync();
    })
    .tap(function() {
      console.log('Listening on port ' + server.info.port + ' - ' + server.settings.app.mode + ' mode');
    })
;




