'use strict';

var path = require('path');
var Promise = require('bluebird');
var glob = Promise.promisify(require('glob'));
var elasticsearch = require('elasticsearch');

var conf = require('../conf');
var client = module.exports.client = new elasticsearch.Client(conf.elasticsearch);

function cmp(a, b) {
  return a < b ?
    -1 :
    a === b ?
      0 :
      1
  ;
}

module.exports.initIndex = function() {
  return Promise.try(function() {
      return client.indices.create({
        index: conf.elasticsearch.index,
        body: conf.elasticsearch.indexInitParams,
        ignore: 400,
      })
    })
    .then(function() {
      return glob('**/*.esobject.js', {cwd: path.join(__dirname, '..')});
    })
    .call('sort', function(a, b) {
      var aLength = a.split('/').length;
      var bLength = b.split('/').length;
      return -cmp(aLength, bLength) || cmp(a, b);
    })
    .each(function(filename) {
      var Type = require(path.join(__dirname, '..', filename));
      return Type
        .createOrUpdateMapping()
        .tap(function() {
          console.log('Mapping: ' + Type.dbConfig({}).type);
        })
      ;
    })
    .return(null)
  ;
};
