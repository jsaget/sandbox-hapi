'use strict';

var esobject = require('esobject');
var uuid = require('node-uuid');
var conf = require('../../conf');
var client = require('../../es').client;

module.exports = esobject.create({
  db: {
    client: client,
    index: conf.elasticsearch.index,
    type: 'token',
  },

  mapping: __dirname + '/token.mapping.yaml',
  import: {
    _id: {$default: uuid.v4},
    username: {$id: 'keepold'},
    createdAt: {$default: Date.now},
    updatedAt: Date.now,
    deletedAt: undefined,
    _version: {$id: true},
  },
  export: {
    _id: {$id: true},
    _version: {$id: true},
  },
});
