'use strict';

module.exports = {
  method: 'GET',
  path: '/api/auth/logout',
  config: {
    handler: function(request, reply) {
      Promise.try(function() {
          request.yar.reset();
        })
        .then(function() {
          return reply.redirect('/');
        })
      ;
    }
  }
};
