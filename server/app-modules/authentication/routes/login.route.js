'use strict';

var Promise = require('bluebird');

module.exports = {
  method: 'POST',
  path: '/api/auth/login',
  config: {
    handler: function(request, reply) {
      Promise.try(function() {
          var username = request.payload.username;
          var password = request.payload.password;

          if (password === 'password' && username === 'username') {
            request.yar.set('session', { username: username });
            return '/';
          } else {
            return '/?err=badcred';
          }
        })
        .then(function(res) {
          console.log('login.route.js:14', 'REDIRECT');
          return reply.redirect(res);
        })
      ;
    }
  }
};
