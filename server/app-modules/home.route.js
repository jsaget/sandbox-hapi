'use strict';

var Promise = require('bluebird');

module.exports = {
  method: 'GET',
  path: '/',
  config: {
    handler: function (request, reply) {
      Promise.try(function() {
          return request.yar.get('session');
        })
        .then(function(session) {
          if (session && session.username) {
            return '<html><head><title>Login page</title></head><body><h3>Welcome ' +
              session.username +
              '!</h3><br/><form method="get" action="/api/auth/logout">' +
              '<input type="submit" value="Logout">' +
              '</form></body></html>';
          } else if (request.url.query.err && request.url.query === 'badcred') {
            return '<html><head><title>Login page</title></head><body>Bad credentials</body></html>';
          } else {
            return '<html><head><title>Login page</title></head><body>' +
              '<form method="post" action="/api/auth/login">' +
              'username: <input type="text" name="username"><br/>' +
              'password: <input type="text" name="password"><br/>' +
              '<input type="submit" value="Login"><br/>' +
              '</form></body></html>';
          }
        })
        .then(reply)
      ;
    }
  }
};
