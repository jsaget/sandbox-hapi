'use strict';

var autoprefixer = require('autoprefixer-stylus');
var glou = require('glou');
var nib = require('nib');
var rupture = require('rupture');
var path = require('path');
var config = require('./configFileReader');
var env = require('./cliParams');
var $ = require('./plugins');

var initSourceMap = glou
  .pipe('init sourcemaps', $.if, env.sourcemaps, [$.sourcemaps.init, {loadMaps: true}])
;

var stylesLib = initSourceMap
  .pipe('remove sourcemaps', $.if, env.removeLibSourcemaps, $.removeSourcemaps)
;

var stylesApp = initSourceMap
  .pipe('csslint', {error: env.dev ? 'warn' : 'fail'}, $.if, '**/*.css', $.csslint)
  .pipe('stylus', function() {
    return $.if('**/*.styl', $.stylus({
      use: [nib(), rupture(), autoprefixer({browsers: 'ie 8'})],
      compress: true,
      sourcemap: {inline: true},
    }));
  })
  .pipe('cssmin', $.if, env.uglify, $.cssmin)
;

var styles = glou
  .parallel(
    glou.src(function() {
      return this.config('styles').lib;
    })
    .pipe(stylesLib),
    glou.src(function() {
      return this.config('styles').app;
    })
    .pipe(stylesApp)
  )
  .remember()
  .pipe('concat', $.concat, 'styles.min.css')
  .pipe('write sourcemaps', $.if, env.sourcemaps, $.sourcemaps.write)
  .dest(function() {
    return path.resolve(__dirname, '..', 'build', this.config('frontName'), 'css');
  })
;

module.exports.buildStyles = function(frontName, cfg) {
  glou.task(frontName + '-style', glou
    .configure(cfg)
    .pipe(styles)
  );
};
