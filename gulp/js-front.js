'use strict';

var path = require('path');
var glou = require('glou');

var env = require('./cliParams');
var $ = require('./plugins');
var check = require('./js-check');
var template = require('./template');

var initSourceMap = glou
  .pipe('init sourcemaps', env.sourcemaps ? $.sourcemaps.init : $.noop, {loadMaps: true})
;

var jsClientLib = initSourceMap
  .pipe('remove sourcemaps', env.removeLibSourcemaps ? $.removeSourcemaps : $.noop)
  .pipe('uglify', env.uglify ? $.if : $.noop, '*.min.js', $.noop, [$.uglify, {mangle: !env.dev}])
  .remember(function() {
    return this.config('frontName') + '-' + this.config('type') + '-lib';
  })
;

var jsClientApp = initSourceMap
  .pipe('jsCheck', check)
  .pipe('ng-annotate', $.ngAnnotate)
  .pipe('wrap-commonjs', function() {
    var frontName = this.config('frontName');

    return $.wrapCommonjsSourcemaps({
      pathModifier: function(filepath) {
        var res = path.relative(__dirname + '/../' + frontName, filepath)
          .replace(/\.js$/, '')
          .replace(new RegExp('\\' + path.sep, 'g'), '/')
        ;
        res = res[0] === '.' ? res : './' + res;
        return res;
      }.bind(this),
    });
  })

  .pipe('uglify', env.uglify ? $.uglify : $.noop, {mangle: !env.dev})
  .append(glou.src('gulp/templates/require.js').pipe(initSourceMap))
  .remember(function() {
    return this.config('frontName') + '-' + this.config('type') + '-app';
  })
  .pipe('concat', $.concat, glou.config('filename'))
  //.pipe('wrap', $.wrap, {append: 'require("./index");'})
;

var jsClient = glou.parallel(
  glou
    .src(function() {
      console.log('js-front.js:53', this.config('js').lib[this.config('type')]);
      return this.config('js').lib[this.config('type')];
    })
    .pipe('jsClientLib', jsClientLib),
  glou
    .src(function() {
      return this.config('js').app[this.config('type')];
    })
    .pipe('jsClientApp', jsClientApp)
);

var pipeJsClientWriter = glou
  .pipe('Concat', $.concat, glou.config('filename'))
  .pipe('write sourcemaps', env.sourcemaps ? $.sourcemaps.write : $.noop)
  // .dest('build/' + this.config('frontName') + '/js')
  .dest(function() {
    return path.resolve(__dirname, '..', 'build', this.config('frontName'), 'js');
  })
;

var jsClientHead = glou
  .configure({
    type: 'head',
    filename: 'head.min.js',
  })
  .pipe(jsClient)
  .pipe('jsClientWrite', pipeJsClientWriter)
;

var jsClientBody = glou
  .configure({
    type: 'body',
    filename: 'body.min.js',
  })
  .parallel(jsClient, template)
  .pipe('jsClientWrite', pipeJsClientWriter)
;

var buildFront = glou.parallel([jsClientHead, jsClientBody]);

module.exports.buildFront = function(frontName, cfg) {
  glou.task(frontName + '-jsfront', glou.pipe(buildFront, cfg));

  glou.task(frontName + '-jscheck', glou
    .configure(cfg)
    .src(function() {
      var files = this.config('js').app.head || [];
      var bodyFiles = this.config('js').app.body;

      if (bodyFiles)
        Array.prototype.push.apply(files, bodyFiles);

      return files;
    })
    .pipe('CHECK', check)
  );
};

