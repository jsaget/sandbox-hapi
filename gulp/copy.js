'use strict';

var _ = require('lodash');
var path = require('path');
var glou = require('glou');

// copy = [{
//  from: '',
//  to: '',
//  srcOptions: { /* see glou.src option */ }
// }, {...}]
var pipeCopy = glou.parallel(function() {
    var frontName = this.config('frontName');
    return _.map(this.config('copy', []), function(copy) {
      var options = _.extend({buffer: false}, copy.srcOptions || {});
      return glou.src(options, copy.from)
        .dest(path.resolve(__dirname, '../build', frontName, copy.to));
    });
  }
);

module.exports.copy = function(frontName, cfg) {
  glou.task(frontName + '-copy', glou
    .configure(cfg)
    .pipe(pipeCopy));
};
