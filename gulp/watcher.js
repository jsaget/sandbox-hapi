'use strict';

var glou = require('glou');
var gulp = require('gulp');
var _ = require('lodash');

var config = require('./configFileReader');
var server = require('./js-server');

module.exports = gulp
  .task('watch', function() {
    _.each(config.client, function(value, frontName) {
      // js watcher
      var jsClientFiles = [config.i18n];
      jsClientFiles.push(config.clients[frontName].js.app.head);
      jsClientFiles.push(config.clients[frontName].js.app.body);
      jsClientFiles.push(config.clients[frontName].templates.files);

      glou.watch(jsClientFiles, [frontName + '-jsfront']);

      // style watcher
      var stylesFiles = [];
      stylesFiles.push(config.clients[frontName].styles.app);
      glou.watch(stylesFiles, frontName + '-style');
    });

    // servers watcher
    server.watcher();
  })
;
