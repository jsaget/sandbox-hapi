'use strict';

var path = require('path');
var glou = require('glou');
var $ = require('./plugins');
var env = require('./cliParams');

var compileTemplates = glou
  .pipe('ng-templates', $.ngTemplate, function() {
    return {
      moduleName: this.config('templates').module,
      filePath: 'templates.js'
    };
  })
  .pipe('wrap', $.wrap, true)
  .pipe('uglify', $.uglify)
  .pipe('init sourcemaps', $.if, env.sourcemaps, [$.sourcemaps.init, {loadMaps: true}])
;

var writeHtml = glou
  .dest(function() {
    return path.join('build', this.config('frontName'));
  })
  .pipe($.swallow)
;

module.exports = glou
  .src(function() {
    return this.config('templates').files;
  })
  .pipe('jade', $.if, '**/*.jade', [$.jade, {doctype: 'html'}])
  // .pipe('htmlmin', $.minifyHtml)
  .remember()
  .pipe($.if, '**/*.tpl.{html,jade}', compileTemplates, writeHtml)
;
