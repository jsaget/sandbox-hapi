'use strict';

var glou = require('glou');
var $ = require('./plugins');
var env = require('./cliParams');

// Glou object config for JSHINT:
// jshint: {
//  rc: '.jshintrc',
//  reporter: 'jshint-stylish',
//  failReporter: 'fail',
// }
var jshint = module.exports.jshint = glou
  .pipe($.jshint, glou.config('rc', '.jshintrc'))
  .pipe($.jshint.reporter, glou.config('reporter', 'jshint-stylish'))
  .pipe({error: env.dev ? 'warn' : 'fail'}, $.jshint.reporter, glou.config('failReporter', 'fail'))
;


// Glou object config for JSCS:
// jscs: {
//  rc: '.jscsrc',
//  failReport: 'fail'
// }
var jscs = module.exports.jscs = glou
  .pipe(function() {
    var path = this.config('rc');
    if (path)
      return $.jscs({configPath: path});
    return $.jscs();
  })
  .pipe($.jscs.reporter)
  .pipe({error: env.dev ? 'warn' : 'fail'}, $.jscs.reporter, glou.config('failReporter', 'fail'))
;

module.exports = glou
  .pipe('jshint', jshint, glou.config('jshint'))
  .pipe('jscs', {error: env.dev ? 'warn' : 'fail'}, jscs, glou.config('jscs'))
;
