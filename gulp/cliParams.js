'use strict';

var argv = require('yargs').argv;

module.exports = {
  jsCheck: argv.nocheck || argv.dev ? false : true,
  uglify: argv.nouglify || argv.dev ? false : true,
  sourcemaps: argv.sourcemaps || argv.dev,
  removeLibSourcemaps: true,
};


// module.exports = {
//   dev: argv.dev,
//   uglify: !argv.ie8,
//   sourcemaps: argv.dev || argv.sourcemaps,
//   removeLibSourcemaps: true,
// };
