'use strict';

const Promise = require('bluebird');
var gulp = require('gulp');
var glou = require('glou');
var check = require('./js-check');
var path = require('path');
var fs = Promise.promisifyAll(require('fs'));
var spawn = require('child_process').spawn;
var path = require('path');
var chalk = require('chalk');
var lr = require('tiny-lr-fork');
var $ = require('./plugins');
var config = require('./configFileReader');

// indexPath: path to the main JS server file
function Server(indexPath, cfg) {
  this.indexPath = indexPath;
  this.cfg = cfg;

  process.stdin.setEncoding('utf8');
  process.stdin.on('data', function(data) {
    data = (data + '').trim().toLowerCase();
    if (data === 'rs') {
      this.restart();
    }
    if (data === 'stop' || data === 'exit') {
      this.listening = false;
      this.instance.kill('SIGTERM');
      process.exit();
    }
  }.bind(this));
}

Server.prototype.start = function() {
  this.listening = false;

  fs.accessAsync(path.resolve(__dirname, '..', 'build'), fs.F_OK)
    .then(function(error) {
      if (error) {
        console.log('"gulp build" must be call before run/start server.');
        process.exit();
      }
    })
    .then(function() {
      $.log(chalk.gray('starting server…'));

      var stdio = [process.stdin, 'pipe', process.stderr];
      this.instance = spawn('node', ['--debug', path.resolve(this.indexPath)], {stdio: stdio, cwd: 'build/'});

      $.log(chalk.green('server started') + chalk.gray(' (' + this.instance.pid + ')'));
      $.log(chalk.yellow('to restart at any time, enter `rs`'));
      $.log(chalk.yellow('to stop server at any time, enter `stop`'));

      process.stdin.resume();

      this.instance.on('close', function() {
        $.log(chalk.red('server closed') + chalk.gray(' (' + this.instance.pid + ')'));
        this.instance = null;
        if (this.listening)
          this.start();
      }.bind(this));

      this.instance.stdout.pipe(process.stdout);
      var setListening = function(data) {
        if (!data.toString('utf8').match('Listening'))
          return;

        this.listening = true;
        this.instance.stdout.removeListener('data', setListening);
      }.bind(this);

      this.instance.stdout.on('data', setListening);

      if (!this.lr) {
        this.lr = lr();
        var port = this.cfg && this.cfg.lr && this.cfg.lr.port ?
          this.cfg.lr.port :
          35729;
        this.lr.listen(port, function() {
          $.log(chalk.grey('Live reload server started on port ' + port + '…'));
        }.bind(this));
      }
    }.bind(this))
  ;
};

Server.prototype.restart = function() {
  if (!this.instance)
    return this.start();

  $.log(chalk.gray('restarting server… (' + this.instance.pid + ')'));
  this.instance.kill('SIGTERM');
};

Server.prototype.newChanges = function(files) {
  this.lr.changed({body: {files: files}});
};

module.exports.startInstance = function(cfg) {
  var serverInstance = null;

  glou.task('server-jscheck', glou
    .configure(cfg)
    .src(glou.config('files'))
    .pipe('CHECK', check)
  );

  gulp.task('server-start', function() {
    if (!serverInstance)
      serverInstance = new Server(config.server.entry, {});

    serverInstance.restart();
  });

  module.exports.watcher = function() {
    gulp.watch(config.server.files, function() {
      if (!serverInstance)
        return;
      serverInstance.restart();
    });
  };
};
