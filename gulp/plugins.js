'use strict';

var _ = require('lodash');
var glou = require('glou');
var path = require('path');

var plugins = _.extend(
  module.exports,
  glou.plugins,
  glou.plugins.loadPlugins({
    lazy: false,
    config: path.resolve(__dirname, '../package.json')
  }),
  {
    through: require('through2'),

    // FIXME: handle sourcemaps + export in a plugin
    header: function(header) {
      return plugins.through.obj(function(file, encoding, next) {
        file.contents = Buffer.concat([new Buffer(header), file.contents]);
        next(null, file);
      });
    },

    // FIXME: handle sourcemaps + export in a plugin
    footer: function(footer) {
      return plugins.through.obj(function(file, encoding, next) {
        file.contents = Buffer.concat([file.contents, new Buffer(footer)]);
        next(null, file);
      });
    },

    removeSourcemaps: function() {
      return plugins.through.obj(function(file, enc, cb) {
        file.contents = new Buffer(file.contents.toString(enc).replace(/^\/\/#.*?$/gm, ''), enc);
        cb(null, file);
      });
    }
  }
);

plugins.wrap = glou
  .configure(function() {
    var replace = '!function(){';

    if (this.config('strict', true))
      replace += "'use strict';";
    replace += '\n$0\n' + this.config('prepend', '') + '\n}();\n';

    return {replace: replace};
  })
  .pipe(plugins.regexpSourcemaps, /^((.|\n)*)$/, glou.config('replace'), 'iife')
;
