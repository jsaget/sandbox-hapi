'use strict';

var fs = require('fs');
var path = require('path');
var yaml = require('js-yaml');

// Read yaml config file
module.exports = yaml.safeLoad(
  fs.readFileSync(path.join(__dirname, '..', 'config.yaml'), 'utf8')
);

module.exports.get = function(key, defaultValue) {
  return _.reduce(key.split('.'), function(acc, attr) {
    return (acc || {})[attr];
  }, this) || defaultValue;
};
